/*
 * runcron - a cron job wrapper with some useful features.
 *
 * Copyright (c) 2014 <ale@incal.net>.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#define _GNU_SOURCE
#include <ctype.h>
#include <errno.h>
#include <getopt.h>
#include <math.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <syslog.h>
#include <time.h>
#include <unistd.h>

#include "config.h"

/* Global options */
char *config_file = "/etc/runcron.conf";
char *job_name = NULL;
char *node_exporter_dir = "/var/lib/prometheus/node-exporter";
int splay_time = 0;
int run_timeout = 0;
int log_to_syslog = 1;
int locking = 1;
int lock_timeout = 0;
int lock_wait = 0;
int do_export_metrics = 1;

char *lock_dir = "/var/lock";
char *log_tag;

struct timespec one_second = {.tv_sec = 1, .tv_nsec = 0};

time_t start_time, end_time;

int usage() {
  fprintf(
      stderr,
      "%s v%s - a cron job wrapper\n"
      "Usage: %s [<OPTIONS>] [--] <COMMAND> [<ARGS>...]\n"
      "\n"
      "Known options:\n"
      "\n"
      "   --help            Show this help message\n"
      "   --splay SECONDS   Wait a random amount of time between 0 and SECONDS "
      "before\n"
      "                     actually starting the command.\n"
      "   --timeout SECONDS\n"
      "                     If the command runs for more than the specified "
      "amount of\n"
      "                     time, terminate it. By default commands can run "
      "forever.\n"
      "\n"
      "Options controlling the command output:\n"
      "\n"
      "   --no-syslog       Do not send command output to syslog.\n"
      "\n"
      "Options controlling locking behavior:\n"
      "\n"
      "   -n, --name NAME   Set the job name. By default, the basename of "
      "COMMAND will\n"
      "                     be used.\n"
      "   --no-lock         Disable locking, allow running multiple instances "
      "at once.\n"
      "   --wait            Wait for the lock to be released. If another "
      "instance of\n"
      "                     this command is running, wait until it terminates "
      "(or\n"
      "                     until the lock timeout expires).\n"
      "   --lock-timeout SECONDS\n"
      "                     Wait at most the specified amount of time for the "
      "instance\n"
      "                     lock to be released, failing with an error if "
      "after SECONDS\n"
      "                     the lock could not be obtained. If set to 0 (the "
      "default),\n"
      "                     wait forever. Specifying this option implies "
      "--wait.\n"
      "   --lock-dir PATH   Directory where to create lock files (default "
      "/var/lock).\n"
      "\n"
      "Options for Prometheus integration:\n"
      "\n"
      "   --node-exporter-dir PATH\n"
      "                    Look for the prometheus-node-exporter state "
      "directory "
      "in PATH\n"
      "                    (default /var/lib/prometheus/node-exporter). If the "
      "directory\n"
      "                    exists, the program will write a small "
      "node-exporter "
      "textfile\n"
      "                    snippet in there.\n"
      "\n"
      "If you are passing options to COMMAND, remember to use `--' to tell the "
      "runcron\n"
      "option parser to stop interpreting options for itself.\n"
      "\n"
      "On startup, runcron will load a system-wide configuration file from \n"
      "/etc/runcron.conf (overridable by setting the environment variable\n"
      "RUNCRON_CONFIG). This file can contain command-line options (without "
      "the "
      "leading\n"
      "--), one per line, with the optional argument separated by a space. "
      "Options set\n"
      "on the command line take precedence over those specified in the "
      "configuration\n"
      "file.\n"
      "\n",
      PACKAGE, PACKAGE_VERSION, PACKAGE);
  return 2;
}

void log_err(const char *format, ...) {
  va_list args, dup_args;
  va_start(args, format);
  if (log_to_syslog) {
    va_copy(dup_args, args);
    vsyslog(LOG_ERR, format, dup_args);
    va_end(dup_args);
  }
  va_copy(dup_args, args);
  fprintf(stderr, "%s: ", log_tag);
  vfprintf(stderr, format, dup_args);
  va_end(dup_args);
  fputc('\n', stderr);
  va_end(args);
}

int to_int(char *s) {
  long n;
  char *endptr = NULL;
  errno = 0;
  n = strtol(s, &endptr, 10);
  if (errno != 0 || *endptr != '\0') {
    return -1;
  }
  return n;
}

/* hashpjw implementation, based on glibc/intl/hash-string.c */
void hash_add(unsigned long *hvalp, const char *s) {
  unsigned long hval = *hvalp, g;
  unsigned const char *sptr = (unsigned const char *)s;
  for (; *sptr; sptr++) {
    hval <<= 4;
    hval += *sptr;
    g = hval & ((unsigned long)0xf << (32 - 4));
    if (g != 0) {
      hval ^= g >> (32 - 8);
      hval ^= g;
    }
  }
  *hvalp = hval;
}

/* Convert a floating-point time (seconds) to a struct timespec. */
void time_to_spec(float t, struct timespec *out) {
  int secs = (int)floor(t);
  out->tv_sec = (time_t)secs;
  out->tv_nsec = (long)floor((t - (float)secs) * 1e9);
}

/* Return a time in the range [0, mod), based on the given hash value. */
float hash_time(unsigned long hval, float mod) {
  /* Use the lower 16 bits to obtain a value in the [0,1) range. */
  static const unsigned long maxhashbits = (1 << 16) - 1;
  float f = (float)(hval & maxhashbits) / (float)maxhashbits;
  return mod * f;
}

void splay(char **argv) {
  /* The splay time should be different for every host / job
   * combination, but it should also be only a function of those
   * parameters. To achieve this and also guarantee a reasonable
   * spread over the time space, we use a very simple hash of hostid +
   * command line.
   */
  unsigned long hval = gethostid();
  struct timespec sleep_time;
  for (; *argv; argv++) {
    hash_add(&hval, *argv);
  }

  time_to_spec(hash_time(hval, splay_time), &sleep_time);
  nanosleep(&sleep_time, NULL);
}

void lock_wait_expired(int signum) {
  log_err("Error: could not acquire lock within the timeout");
  exit(3);
}

void lock_with_timeout() {
  char *lockfile = NULL;
  sighandler_t old_handler = NULL;
  int fd, r, lock_flags;

  if (asprintf(&lockfile, "%s/runcron.%s.lock", lock_dir, job_name) < 0) {
    log_err("Error: could not allocate memory");
    exit(1);
  }

  /* We open the lock file but never close it: the file descriptor
   * (and the associated flock) will be automatically released by the
   * kernel once this program exits.
   */
  fd = open(lockfile, O_RDWR | O_CREAT, 0600);
  if (fd < 0) {
    log_err("Error: could not open lock file: %s", strerror(errno));
    exit(1);
  }

  lock_flags = LOCK_EX;
  if (lock_wait) {
    if (lock_timeout > 0) {
      old_handler = signal(SIGALRM, lock_wait_expired);
      alarm(lock_timeout);
    }
  } else {
    lock_flags |= LOCK_NB;
  }

  r = flock(fd, lock_flags);
  if (r < 0) {
    if (errno == EWOULDBLOCK) {
      log_err("Error: another instance is already running");
    } else {
      log_err("Error: lock could not be obtained: %s", strerror(errno));
    }
    exit(3);
  }
  if (lock_wait && lock_timeout > 0) {
    signal(SIGALRM, old_handler);
    alarm(0);
  }

  free(lockfile);
}

int start_logger() {
  int pipefd[2], pid, r;

  r = pipe(pipefd);
  if (r < 0) {
    log_err("Error: could not create log pipe: %s", strerror(errno));
    return -1;
  }

  pid = fork();
  if (pid < 0) {
    log_err("Error: could not spawn logger: %s", strerror(errno));
    return -1;
  } else if (pid == 0) {
    int i;
    char *logger_args[] = {
        "/usr/bin/logger", "-p", "daemon.info", "-t", log_tag, NULL,
    };
    /* Put the pipe on stdin, close all other fds. */
    dup2(pipefd[0], 0);
    for (i = 3; i < 1024; i++) {
      close(i);
    }
    r = execvp(logger_args[0], logger_args);
    if (r < 0) {
      log_err("Error: logger: %s", strerror(errno));
    }
    exit(1);
  }

  close(pipefd[0]);
  return pipefd[1];
}

void dumpfd(int from, int to) {
  char buf[4096];
  int n;

  lseek(from, 0, SEEK_SET);
  while (1) {
    n = read(from, buf, sizeof(buf));
    if (n <= 0) {
      break;
    }
    n = write(to, buf, n);
    if (n < 0) {
      break;
    }
  }
}

/* State for the process terminator. */
int child_pid = -1;
int termination_signal = SIGTERM;
volatile int timeout_expired = 0;
sigset_t parent_sigset, child_sigset;

int wait_and_forward_signals(int *status) {
  siginfo_t sig;

  while (1) {
    // Wait for a signal.
    if (sigtimedwait(&parent_sigset, &sig, &one_second) < 0) {
      switch (errno) {
      case EAGAIN:
        break;
      case EINTR:
        break;
      default:
        log_err("Error: unexpected error in sigtimedwait(): %s",
            strerror(errno));
        return -1;
      }
    } else {
      switch (sig.si_signo) {
      case SIGCHLD:
        // Fall through to reaping the child process.
        break;

      case SIGALRM:
        // Timeout expired, kill the child process.
        if (kill(child_pid, termination_signal) < 0) {
          if (errno == ESRCH) {
            // Process is already gone, reap it.
            break;
          }
          log_err("Error: sending signal %d to pid %d: %s",
              sig.si_signo, child_pid, strerror(errno));
          return -1;
        }
        // Retry in 3 seconds with SIGKILL.
        timeout_expired = 1;
        termination_signal = SIGKILL;
        alarm(3);

      default:
        // Send signal to child pid.
        if (kill(child_pid, sig.si_signo) < 0) {
          if (errno == ESRCH) {
            // Process is already gone, reap it.
            break;
          }
          log_err("Error: sending signal %d to pid %d: %s",
              sig.si_signo, child_pid, strerror(errno));
          return -1;
        }
        continue;
      }
    }

    // Call waitpid().
    if (waitpid(child_pid, status, WNOHANG) > 0) {
      return 0;
    }
  }
}

int setup_signals() {
  // Signals that should bypass the main loop.
  int i, signals_to_skip[] = {
             SIGFPE,  SIGILL, SIGSEGV, SIGBUS,  SIGABRT,
             SIGTRAP, SIGSYS, SIGTTIN, SIGTTOU,
         };

  // Set up the signal mask, before forking.
  if (sigfillset(&parent_sigset)) {
    log_err("Error: sigfillset() failed: %s", strerror(errno));
    return -1;
  }

  for (i = 0; i < sizeof(signals_to_skip) / sizeof(int); i++) {
    if (sigdelset(&parent_sigset, signals_to_skip[i])) {
      log_err("Error: sigdelset() failed: %s", strerror(errno));
      return -1;
    }
  }

  if (sigprocmask(SIG_SETMASK, &parent_sigset, &child_sigset)) {
    log_err("Error: sigprocmask() failed: %s", strerror(errno));
    return -1;
  }

  return 0;
}

int run(char **argv) {
  int r, wait_status, exit_status = -1, logfd = -1;

  if (setup_signals() < 0) {
    return -1;
  }

  // To send the command output to syslog, create a pipe and spawn
  // '/usr/bin/logger' to read from it.
  if (log_to_syslog) {
    logfd = start_logger();
    if (logfd < 0) {
      return -1;
    }
  }

  start_time = time(NULL);

  child_pid = fork();
  if (child_pid < 0) {
    log_err("Error: fork(): %s", strerror(errno));
  } else if (child_pid == 0) {
    const char *argv0;

    if (logfd >= 0) {
      dup2(logfd, 1);
      dup2(logfd, 2);
    }

    if (sigprocmask(SIG_SETMASK, &child_sigset, NULL)) {
      fprintf(stderr, "Error: sigprocmask(): %s\n", strerror(errno));
      exit(1);
    }

    // execvp() can mangle argv[0] when the program is not found.
    argv0 = strdup(argv[0]);
    r = execvp(argv[0], argv);
    if (r < 0) {
      fprintf(stderr, "Error: exec(%s): %s\n", argv0, strerror(errno));
      exit(1);
    }
  } else {
    if (run_timeout > 0) {
      alarm(run_timeout);
    }

    r = wait_and_forward_signals(&wait_status);
    if (r < 0) {
      return 1;
    } else {
      if (WIFEXITED(wait_status)) {
        exit_status = WEXITSTATUS(wait_status);
      } else if (WIFSIGNALED(wait_status)) {
        if (timeout_expired) {
          log_err("Error: %s execution timed out", argv[0]);
          exit_status = 142;
        } else {
          log_err("Error: %s terminated by signal %d", argv[0],
                  WTERMSIG(wait_status));
          exit_status = 128 + WTERMSIG(wait_status);
        }
      } else {
        log_err("Error: %s terminated for unknown reason", argv[0]);
      }
    }
  }

  if (logfd >= 0) {
    if (exit_status != 0) {
      /* Dump the actual output to stderr. */
      dumpfd(logfd, 2);
    }
  }

  return exit_status;
}

/* This function returns a boolean value. */
static int prometheus_node_exporter_enabled() {
  struct stat sb;
  if (stat(node_exporter_dir, &sb) == 0 && ((sb.st_mode & S_IFMT) == S_IFDIR)) {
    return 1;
  }
  return 0;
}

static void print_prom_metric(FILE *fp, const char *name, const char *help,
                              const char *type, long value) {
  fprintf(fp, "# HELP %s %s\n"
              "# TYPE %s %s\n"
              "%s{cronjob=\"%s\"} %ld\n",
          name, help, name, type, name, job_name, value);
}

/* Export metrics to the prometheus-node-exporter using its textfiles module.
 * We create two separate files, one with per-run metrics and one with just
 * the success_timestamp metric, so that the latter is not overwritten on
 * failure, and the filesystem keeps the historical state for us.
 */
typedef void (*write_metrics_fn)(FILE *, int);

int write_file(const char *path, write_metrics_fn wfn, int exit_status) {
  char *tmp_file = NULL;
  FILE *fp;

  if (asprintf(&tmp_file, "%s.%d.tmp~", path, getpid()) < 0) {
    log_err("Error: could not allocate memory");
    return -1;
  }

  fp = fopen(tmp_file, "wx");
  if (fp == NULL) {
    log_err("Error: could not open %s: %s", path,
        strerror(errno));
    goto fail;
  }

  wfn(fp, exit_status);
  fclose(fp);

  if (rename(tmp_file, path) < 0) {
    log_err("Error: could not write file: %s", strerror(errno));
    goto fail;
  }

  free(tmp_file);
  return 0;

 fail:
  unlink(tmp_file);
  free(tmp_file);
  return -1;
}

void write_base_metrics(FILE *fp, int exit_status) {
  int elapsed;

  end_time = time(NULL);
  elapsed = end_time - start_time;
  print_prom_metric(fp, "cronjob_ok", "Status of the cron job.", "gauge",
                    (exit_status == 0) ? 1 : 0);
  print_prom_metric(fp, "cronjob_last_start", "Last cron job start time.",
                    "gauge", start_time);
  print_prom_metric(fp, "cronjob_last_end", "Last cron job end time.", "gauge",
                    end_time);
  print_prom_metric(fp, "cronjob_runtime_seconds",
                    "Duration of the last cron job run.", "gauge", elapsed);
}

void write_success_metrics(FILE *fp, int exit_status) {
  print_prom_metric(fp, "cronjob_last_success",
                      "End time of the last successful cron job.", "gauge",
                      end_time);
}

int export_metrics(int exit_status) {
  char *metrics_file = NULL;

  if (asprintf(&metrics_file, "%s/cron-%s.prom", node_exporter_dir, job_name) <
      0) {
    log_err("Error: could not allocate memory");
    return -1;
  }

  if (write_file(metrics_file, write_base_metrics, exit_status) < 0) {
    return -1;
  }

  free(metrics_file);

  if (exit_status == 0) {
    if (asprintf(&metrics_file, "%s/cron-%s-success.prom", node_exporter_dir, job_name) < 0) {
      log_err("Error: could not allocate memory");
      return -1;
    }

    if (write_file(metrics_file, write_success_metrics, exit_status) < 0) {
      return -1;
    }
    free(metrics_file);
  }

  return 0;
}

char *trim(char *s) {
  char *tail;
  while (isspace(*s))
    s++;
  tail = s + strlen(s);
  tail--;
  while (tail > s && isspace(*tail))
    *(tail--) = '\0';
  return s;
}

int read_config() {
  FILE *fp;
  char *s, linebuf[1024];
  int r = -1, lineno = 0;

  if ((s = getenv("RUNCRON_CONFIG")) != NULL) {
    config_file = s;
  }

  fp = fopen(config_file, "r");
  if (fp == NULL) {
    return -1;
  }

  while (fgets(linebuf, sizeof(linebuf) - 1, fp) != NULL) {
    char *sep, *key, *val;

    lineno++;
    if (linebuf[0] == '\0' || linebuf[0] == '#') {
      continue;
    }
    key = trim(linebuf);
    sep = strchr(linebuf, ' ');
    if (sep == NULL) {
      // Value is the empty string (but not NULL).
      val = linebuf + strlen(linebuf);
    } else {
      *sep = '\0';
      val = trim(++sep);
    }

    if (!strcmp(key, "timeout")) {
      run_timeout = to_int(val);
      if (run_timeout < 0) {
        log_err("Error: %s:%d: timeout value must be an integer",
                config_file, lineno);
      }
    } else if (!strcmp(key, "lock-timeout")) {
      lock_timeout = to_int(val);
      if (lock_timeout < 0) {
        log_err("Error: %s:%d: lock-timeout value must be an integer",
                config_file, lineno);
      }
    } else if (!strcmp(key, "lock-dir")) {
      lock_dir = strdup(val);
    } else if (!strcmp(key, "no-lock")) {
      locking = 0;
    } else if (!strcmp(key, "no-syslog")) {
      log_to_syslog = 0;
    } else if (!strcmp(key, "node-exporter-dir")) {
      node_exporter_dir = strdup(val);
    } else {
      log_err("Error: %s:%d: unknown attribute %s", config_file,
              lineno, key);
    }
  }

  fclose(fp);
  return r;
}

int main(int argc, char **argv) {
  int c, r;

  read_config();

  while (1) {
    int option_index = 0;
    const char *opt_name = NULL;
    static struct option long_options[] = {
        {"help", no_argument, 0, 'h'},
        {"name", required_argument, 0, 'n'},
        {"splay", required_argument, 0, 0},
        {"timeout", required_argument, 0, 0},
        {"no-syslog", no_argument, &log_to_syslog, 0},
        {"no-lock", no_argument, &locking, 0},
        {"nolock", no_argument, &locking, 0},
        {"lock-timeout", required_argument, 0, 0},
        {"lock-dir", required_argument, 0, 0},
        {"wait", no_argument, &lock_wait, 1},
        {"node-exporter-dir", required_argument, 0, 0},
        {"no-metrics", no_argument, &do_export_metrics, 0},
        // Legacy
        {"quiet", no_argument, 0, 'q'},
        {0, 0, 0, 0}};

    c = getopt_long(argc, argv, "hn:", long_options, &option_index);
    if (c == -1) {
      break;
    }

    switch (c) {
    case 0:
      opt_name = long_options[option_index].name;
      if (!strcmp(opt_name, "splay")) {
        splay_time = to_int(optarg);
        if (splay_time < 0) {
          fprintf(stderr, "Error: --splay argument must be an integer\n");
          exit(2);
        }
      } else if (!strcmp(opt_name, "timeout")) {
        run_timeout = to_int(optarg);
        if (run_timeout < 0) {
          fprintf(stderr, "Error: --timeout argument must be an integer\n");
          exit(2);
        }
      } else if (!strcmp(opt_name, "lock-timeout")) {
        lock_timeout = to_int(optarg);
        if (lock_timeout < 0) {
          fprintf(stderr,
                  "Error: --lock-timeout argument must be an integer\n");
          exit(2);
        }
      } else if (!strcmp(opt_name, "lock-dir")) {
        lock_dir = optarg;
      } else if (!strcmp(opt_name, "node-exporter-dir")) {
        node_exporter_dir = optarg;
      }
      break;

    case 'n':
      job_name = optarg;
      break;

    case 'q':
      // Legacy option, ignore.
      break;

    case 'h':
      usage();
      exit(0);
      break;
    case '?':
      exit(usage());
      break;
    }
  }

  /* Check for option consistency. */
  if (!locking) {
    if (lock_wait) {
      fprintf(stderr,
              "Error: --wait and --no-lock should not be specified together\n");
      exit(2);
    }
    if (lock_timeout > 0) {
      fprintf(stderr, "Error: --no-lock and --lock-timeout should not be "
                      "specified together\n");
      exit(2);
    }
  } else {
    if (lock_timeout > 0) {
      lock_wait = 1;
    }
  }

  if (optind >= argc) {
    fprintf(stderr, "Error: no command specified\n");
    exit(2);
  }

  if (job_name == NULL) {
    job_name = basename(argv[optind]);
  }
  log_tag = job_name;
  if (log_to_syslog) {
    openlog(log_tag, LOG_PID, LOG_CRON);
  }

  if (splay_time > 0) {
    splay(argv + optind);
  }

  if (locking) {
    lock_with_timeout();
  }

  r = run(argv + optind);
  if (do_export_metrics && prometheus_node_exporter_enabled()) {
    export_metrics(r);
  }

  return r;
}
